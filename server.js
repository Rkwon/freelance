'use strict';

/*** MODE ***/
var NODE_ENV = process.env.NODE_ENV;
var IS_DEBUG = true;

/*** Requirement ***/
var express = require("express");
var pug = require('pug');
var app = express();
app.set('view engine', 'pug');
var path = __dirname + '/views/';
var router = express.Router();
app.use(router);
app.use(express.static(path + 'style'));
app.locals.basedir = app.get('views');
app.set('views', path);
require('./router/main')(app)
/*** ROUTES ***/
/*
router.use(function (req,res,next) {
  console.log("/" + req.method);
  next();
});
*/

/*
router.get("/",function(req,res){
  //res.sendFile(path + "index.html");
  console.log('GET /');
  res.render('index', jsonContent);
});

app.route("/project/software").get(function(req,res){
   console.log('projects/software')
   res.render('projects/software', jsonContent)
});

router.get("/about",function(req,res){
  res.sendFile(path + "about.html");
});

router.get("/contact",function(req,res){
  res.sendFile(path + "contact.html");
});


app.use("*",function(req,res){
  res.sendFile(path + "404.html");
});
*/
var server = app.listen(8080,function(){
  console.log("Live at Port 8080");
});
