PORT=3000
DOCKER_NAME=freelance

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker build -t rkwon/node-web-app .
docker run --name $DOCKER_NAME -p $PORT:8080 -d rkwon/node-web-app
sleep 3
/usr/bin/open -a "/Applications/Google Chrome.app" "http://localhost:$PORT/service/cloud"
docker logs -f $DOCKER_NAME
