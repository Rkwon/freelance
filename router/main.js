// Read configuration
var fs = require("fs");
var extend = require('util')._extend;
const CONFIG_FOLDER = 'configuration/'
/*** Config ***/
console.log('loading json');


//var configCache = {};


/**
CONFIG LOADING
**/
var serverConfig; //= readConfig(CONFIG_FOLDER + 'server.json');
var commonInfo; //= readConfig(serverConfig.common_information);
var cacheConfig = {};


// NodeJs philosophy is everything is Async
// have to handle async calls
function readConfig(pathToJsons, key,onSuccess, onError)
{
   readConfig(pathToJsons, key, null, onSuccess, onError);
}

function readConfig(pathToJsons, key, json, onSuccess, onError)
{
   if(!(key in cacheConfig))
   {
      // initialisation de la config
      if (json === undefined || json == "")
      {
         console.log('json was null, initialised it');
         json = {};
      }

      if(pathToJsons instanceof Array)
      {
         var pathToJson = pathToJsons[0];
         fs.readFile(pathToJson,'utf8',function(err,content)
         {
            innerContent = JSON.parse(content);
            json = extend(json, innerContent);
            // if finish call success
            // finish if pathToJsons.length == 1
            if (pathToJsons.length == 1)
            {
               cacheConfig[key] = json;
               if(onSuccess)
               {
                  onSuccess(cacheConfig[key]);
               }
            }else
            {
               //continue to read configs from array
               readConfig(pathToJsons.slice(1,pathToJsons.length), key, json, onSuccess, onError);
            }
         });
      }
   }
   else if(onSuccess)
   {
      console.log('found cache for ' + key);
      onSuccess(cacheConfig[key]);
   }
}

/*
function getConfig(key)
{
   if(configCache[key] === undefined)
   {
      console.log('Register key into cache : ' + key);
      for (i=0; i<serverConfig.link_configuration.length; i++)
      {
         x = serverConfig.link_configuration[i];
         if(x.url == key)
         {
            console.log('Register key into cache : ' + key);
            console.log('reading config :'+ x.config);
            var innerConfig = readConfig(x.config);
            configCache[key] = extend(commonInfo, innerConfig);
         }
      }
   }

   return configCache[key];
}*/

module.exports = function(app)
{
   app.get('/', function(req,res)
   {
      var json = {};
      console.log(req.url);
      readConfig(['configuration/common.json', 'configuration/index.json'], req.url, json, function(json){
         res.render('page/index', json); 
         //res.json(json);
      }, null );

      //res.json(indexConfig);
      //res.render('page/index', indexConfig);
      //res.end();
   });

   app.get('/service/software',function(req,res)
   {
      var json = {};
      console.log(req.url);
      readConfig(['configuration/common.json', 'configuration/page-service-software.json'], req.url, json, function(json){
         res.render('page/software', json); 
         //res.json(json);
      }, null );
   });

   app.get('/service/it',function(req,res)
   {
      var json = {};
      console.log(req.url);
      readConfig(['configuration/common.json', 'configuration/page-service-it.json'], req.url, json, function(json){
         res.render('page/it', json); 
         //res.json(json);
      }, null );
   });

   app.get('/service/cloud',function(req,res)
   {
      var json = {};
      console.log(req.url);
      readConfig(['configuration/common.json', 'configuration/page-service-cloud.json'], req.url, json, function(json){
         res.render('page/cloud', json); 
         //res.json(json);
      }, null );
   });
}